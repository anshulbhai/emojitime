#Design Goals
The concern here was to build a emoji picker that is a little more intuitive to use than the standard picker. With the system picker, the **Frequently Used** section contains a list of the recently used emoji regardless of the category that they belong to. This can be useful for finding emoji that is most recently used, but often it will get cluttered with seasonal emoji that pertain to the current time of year, 🎃, 🎄, ☀️ and such. This is an attempt to organize the emoji in each category by their usage. As the emoji is used in a category, it will move up based on the rankings when the category is refreshed. 

#Challenges
* SwiftUI does not like creating a large number of buttons in a grid. This leads to a lot of overhead in layout and resizing. So, for now, the items are shown as text and they can be dragged and dropped to the appropriate location.
* Due to issues with AppKit + SwiftUI on the current OS, the backing store (UserDefaults) is updated on each use of an Emoji. This is less than ideal as it can happen pretty often. Preferably, this will only happen when the app is quit.

#Time Spent
Total time: 4 Hours

Additional time was spent dealing with the nascent nature of SwiftUI and its support in the OS. This was not specific to the challenge, but rather running into bugs in the tools and libraries.

#Build/Run Requirements
* macOS Catalina 10.15.1 or later
* Xcode 11.2 or later

#Tradeoffs
As mentioned above, there are a few tradeoffs in this implementation.

* Requires the latest tools to build and latest OS to run. 
* Clicking an emoji does not add it directly to a text field. Emoji can be dragged into any application
* Usage data is written back into the user's preferences more often than it needs to be. This can be changed when the OS bugs are worked out.


#Other Considerations
This work may have been accomplished more simply with RxSwift or another such framework. 

#Future Direction
Grid layout support from SwiftUI could provide a better experience for such a use case.

