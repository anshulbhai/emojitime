//
//  ContentView.swift
//  EmojiTime
//
//  Created by Anshul Kapoor on 11/30/19.
//  Copyright © 2019 Anshul Kapoor. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var selectorIndex = 0
    @State private var usageInfo = EmojiUse()
    
    var body: some View {
        let sections = EmojiLists.emojiCategories.map{$0.key}.sorted()
        return VStack {
            HStack{
                Picker(selection: $selectorIndex.animation(.easeInOut(duration: 0.25)), label: Text(""))  {
                    ForEach(0 ..< sections.count) { index in
                        EmojiCategoryPicker(category: sections[index])
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                .animation(.none)
                
            }
            .padding(.top, 10)
            
            EmojiPicker(category: sections[selectorIndex], usageInfo: $usageInfo)
            .padding(.bottom, 10)
            Text("\(usageInfo.usageInfo.count)")
        }
        .padding([.horizontal], 10)
        
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
