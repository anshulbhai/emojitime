//
//  EmojiPicker.swift
//  EmojiTime
//
//  Created by Anshul Kapoor on 11/30/19.
//  Copyright © 2019 Anshul Kapoor. All rights reserved.
//

import SwiftUI

struct EmojiPicker: View {
    var category: String
    var options: [String]
    let emojiChunks: [EmojiChunk]
    @Binding var usageInfo: EmojiUse
    
    init(category: String, usageInfo: Binding<EmojiUse>) {
        self.category = category
        options = EmojiLists.getList(section: category) ?? ["😀", "🙃"]
        emojiChunks = createChunks(category: category, chunkBy: 30, usageInfo: usageInfo.wrappedValue)
        _usageInfo = usageInfo
    }
    
    var body: some View {
        VStack {
        ForEach(emojiChunks) { chunk in
            EmojiChunkView(chunk: chunk, usageInfo: self.$usageInfo)
                .padding(3)
            }
        }
    }
}

struct EmojiPicker_Previews: PreviewProvider {
    @State var usageInfo = EmojiUse()
    static var previews: some View {
        EmojiPicker(category: "nature", usageInfo:.constant(EmojiUse()))
    }
}
