//
//  EmojiCategoryPicker.swift
//  EmojiTime
//
//  Created by Anshul Kapoor on 11/30/19.
//  Copyright © 2019 Anshul Kapoor. All rights reserved.
//

import SwiftUI

struct EmojiCategoryPicker: View {
    let category: String
    let beginEmojiIcon: String
    let endEmojiIcon: String
    
    init(category: String) {
        self.category = category
        self.beginEmojiIcon = EmojiLists.getList(section: category)?.first ?? "😀"
        self.endEmojiIcon = EmojiLists.getList(section: category)?.last ?? "☂️"
    }
    
    var body: some View {
            Text("\(self.beginEmojiIcon) \(self.category) \(self.endEmojiIcon)")
    }
}

struct EmojiCategoryPicker_Previews: PreviewProvider {
    static var previews: some View {
        EmojiCategoryPicker(category: "nature")
    }
}
