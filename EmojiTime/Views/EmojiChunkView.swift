//
//  EmojiChunkView.swift
//  EmojiTime
//
//  Created by Anshul Kapoor on 11/30/19.
//  Copyright © 2019 Anshul Kapoor. All rights reserved.
//

import SwiftUI

struct EmojiChunkView: View {
    let chunk: EmojiChunk
    @Binding var usageInfo: EmojiUse
    
    init(chunk: EmojiChunk,usageInfo: Binding<EmojiUse>) {
        self.chunk = chunk
        self._usageInfo = usageInfo
    }
    
    var body: some View {
        HStack{
            ForEach(chunk.chunk) { emoji in
                Text(emoji.stringRepresentation)
                    .font(.headline)
                    .onDrag {
                        self.usageInfo.addUsage(emoji: emoji.character)
                        return NSItemProvider(object: emoji.stringRepresentation as NSString)
                }
            }
        }
    }
}

struct EmojiChunk_Previews: PreviewProvider {
    static var previews: some View {
        EmojiChunkView(chunk: createChunks(category: "nature", chunkBy: 10, usageInfo: EmojiUse()).first!, usageInfo:.constant(EmojiUse()))
    }
}
