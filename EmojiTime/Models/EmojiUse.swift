//
//  EmojiUse.swift
//  EmojiTime
//
//  Created by Anshul Kapoor on 11/30/19.
//  Copyright © 2019 Anshul Kapoor. All rights reserved.
//

import Foundation

class EmojiUse: ObservableObject {
    @Published var usageInfo: [String : Int]
    
    init() {
        usageInfo = UserDefaults.standard.value(forKey: "EmojiUsage") as? [String: Int] ?? [:]
    }
    
    func addUsage(emoji: Character) {
        self.objectWillChange.send()
        let emojiString = String(emoji)
        let count = usageInfo[emojiString] ?? 0
        usageInfo.updateValue(count+1, forKey: emojiString)
        UserDefaults.standard.set(usageInfo, forKey: "EmojiUsage")
    }
    
    func checkUsage(emoji: Character) -> Int {
        return self.checkUsage(emoji: String(emoji))
    }
    
    func checkUsage(emoji: String) -> Int {
        self.objectWillChange.send()
        return usageInfo[emoji] ?? 0
    }
}
