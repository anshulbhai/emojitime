//
//  Emoji.swift
//  EmojiTime
//
//  Created by Anshul Kapoor on 11/30/19.
//  Copyright © 2019 Anshul Kapoor. All rights reserved.
//

import Foundation

struct Emoji: Identifiable {
    var id: Character { character }
    let character: Character
    
    init(character: Character) {
        self.character = character
    }
    
    var stringRepresentation: String {
        String(character)
    }
}

struct EmojiChunk: Identifiable {
    var id: String { chunk.reduce("", { $0 + String($1.character)}) }
    let chunk: [Emoji]
    init(emoji: [String]) {
        chunk = emoji.map( { Emoji(character: Character($0)) })
    }
}

extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}

func createChunks(category: String, chunkBy: Int, usageInfo: EmojiUse) -> [EmojiChunk] {
    let emojiOfCategory = EmojiLists.getList(section: category) ?? []
    let sortedEmoji = emojiOfCategory.sorted(by: { left, right in
        let usageLeft = usageInfo.checkUsage(emoji: left)
        let usageRight = usageInfo.checkUsage(emoji: right)
        if (usageLeft != usageRight  ) {
            return usageLeft > usageRight
        }
        return false
    })
    return sortedEmoji.chunked(into: chunkBy).map{EmojiChunk(emoji: $0) }
}
